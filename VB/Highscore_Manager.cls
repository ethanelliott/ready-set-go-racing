VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Highscore_Manager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
    Option Explicit
Private Scores() As Long
Private Names() As String
Private NumberEntries As Long

'Check Highscore
Public Function CheckScore(ByVal Score As Long, Optional ByVal SortDirection As Boolean = True) As Boolean
    Dim i As Long, j As Long
    
    For i = 0 To NumberEntries
        If SortDirection Then 'Higher the better
            If Score > Scores(i) Then
                CheckScore = True
                Exit Function
            End If
        Else
            If Score < Scores(i) Then
                CheckScore = True
                Exit Function
            End If
        End If
    Next i
End Function

'Save Score
Public Function SaveScore(ByVal ApplicationName As String, ByVal PlayerName As String, ByVal Score As Long, Optional ByVal SortDirection As Boolean = True) As Boolean
    Dim i As Long, j As Long
    Dim lT1 As Long, lT2 As Long, sT1 As String, sT2 As String
    Dim lFound As Long
    
    lFound = -1
    
    For i = 0 To NumberEntries
        If SortDirection Then 'Higher the better
            If Score > Scores(i) Then
                SaveScore = True
                lFound = i
                Exit For
            End If
        Else
            If Score < Scores(i) Then
                SaveScore = True
                lFound = i
                Exit For
            End If
        End If
    Next i
    
    If lFound = -1 Then Exit Function
    
    'Shift around scores and names
    lT2 = Score
    sT2 = PlayerName
    For i = lFound To NumberEntries
        lT1 = Scores(i)
        sT1 = Names(i)
        Scores(i) = lT2
        Names(i) = sT2
        lT2 = lT1
        sT2 = sT1
    Next i
    
    'Save all scores
    For i = 0 To NumberEntries
        Call SaveSetting(ApplicationName, "Highscore", i & "S", Scores(i))
        Call SaveSetting(ApplicationName, "Highscore", i & "N", Names(i))
    Next i
End Function

'Load Scores
Public Function LoadScores(ByVal NumberScores As Long, ByVal DefaultScore As Long, ByVal DefaultName As String, ByVal ApplicationName As String, Optional ByVal SortDirection As Boolean = True)
    Dim i As Long, j As Long, t As Long, ts As String
    
    'Make room for scores
    NumberEntries = NumberScores
    ReDim Scores(NumberScores) As Long
    ReDim Names(NumberScores) As String
    
    'Load them
    For i = 0 To NumberScores
        Scores(i) = GetSetting(ApplicationName, "Highscore", i & "S", DefaultScore)
        Names(i) = GetSetting(ApplicationName, "Highscore", i & "N", DefaultName)
    Next i
    
    'Sort them
    For i = 0 To NumberScores
        For j = 0 To NumberScores
            If SortDirection Then 'Higher the better
                If Scores(i) > Scores(j) Then
                    t = Scores(i)
                    ts = Names(i)
                    Scores(i) = Scores(j)
                    Names(i) = Names(j)
                    Scores(j) = t
                    Names(j) = ts
                End If
            Else
                If Scores(i) < Scores(j) Then
                    t = Scores(i)
                    ts = Names(i)
                    Scores(i) = Scores(j)
                    Names(i) = Names(j)
                    Scores(j) = t
                    Names(j) = ts
                End If
            End If
        Next j
    Next i
End Function

'Get Score
Public Function GetPlayerScore(ByVal Index As Long) As Long
    On Error Resume Next
    GetPlayerScore = Scores(Index)
End Function

'Get Player Name
Public Function GetPlayerName(ByVal Index As Long) As String
    On Error Resume Next
    GetPlayerName = Names(Index)
End Function

