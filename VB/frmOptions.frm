VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOptions 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Options"
   ClientHeight    =   7875
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   6150
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   6150
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClearSettings 
      Caption         =   "RESET"
      Height          =   375
      Left            =   120
      TabIndex        =   26
      Top             =   7320
      Width           =   1095
   End
   Begin VB.Frame fraMobile 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Mobile Control"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   22
      Top             =   2880
      Width           =   5895
      Begin VB.OptionButton optMobileEnable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Enabled"
         Height          =   495
         Left            =   240
         TabIndex        =   24
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton optMobileDisable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Disabled"
         Height          =   375
         Left            =   240
         TabIndex        =   23
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Allow Mobile Control?"
         Height          =   495
         Left            =   240
         TabIndex        =   25
         Top             =   480
         Width           =   5415
      End
   End
   Begin VB.Frame fraDeveloper 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Developer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   12
      Top             =   4800
      Width           =   5895
      Begin VB.OptionButton optDeveloperDisable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Disabled"
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   1200
         Width           =   2415
      End
      Begin VB.OptionButton optDeveloperEnable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Enabled"
         Height          =   495
         Left            =   240
         TabIndex        =   13
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Allow the Developer menu?"
         Height          =   495
         Left            =   240
         TabIndex        =   14
         Top             =   480
         Width           =   5415
      End
   End
   Begin VB.Frame fraBackgroundMusic 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Background Music"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5895
      Begin VB.Frame fraCustomMusic 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   1095
         Left            =   360
         TabIndex        =   17
         Top             =   1080
         Width           =   5415
         Begin VB.CommandButton cmdChooseFileMusic 
            Caption         =   "Choose..."
            Height          =   375
            Left            =   4200
            TabIndex        =   21
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox txtFilePath 
            Height          =   375
            Left            =   1200
            TabIndex        =   20
            Top             =   480
            Width           =   2895
         End
         Begin VB.OptionButton optMusicCustom 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Custom:"
            Height          =   495
            Left            =   240
            TabIndex        =   19
            Top             =   360
            Width           =   975
         End
         Begin VB.OptionButton optMusicDefault 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Default"
            Height          =   375
            Left            =   240
            TabIndex        =   18
            Top             =   0
            Width           =   975
         End
      End
      Begin MSComDlg.CommonDialog CommonDialog 
         Left            =   2640
         Top             =   0
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.OptionButton optBackMusicDisable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Disabled"
         Height          =   375
         Left            =   240
         TabIndex        =   15
         Top             =   2160
         Width           =   2295
      End
      Begin VB.OptionButton optBackMusicEnable 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Enabled"
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Play the music in the background?"
         Height          =   495
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   5415
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   8
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   1785
         Left            =   1545
         TabIndex        =   7
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Sample 2"
         Height          =   1785
         Left            =   645
         TabIndex        =   6
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   375
      Left            =   4920
      TabIndex        =   2
      Top             =   7320
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3720
      TabIndex        =   1
      Top             =   7320
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2520
      TabIndex        =   0
      Top             =   7320
      Width           =   1095
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Apply button click
Private Sub cmdApply_Click()
    printf "New Settings:"
    Settings.Backmusic = optBackMusicEnable.Value
    Settings.Developer = optDeveloperEnable.Value
    Settings.Mobile = optMobileEnable.Value
    If optMusicCustom.Value Then
        printf "Custom Music"
        Settings.backMusicFile = txtFilePath.Text
    Else
        printf "Default Music"
        Settings.backMusicFile = Application.BackgroundMusic
    End If
    Settings.saveSettings
    With frmMain
        If Settings.Backmusic Then
            .wmpBackground.URL = Settings.backMusicFile
            printf "Music Enabled"
        Else
            .wmpBackground.URL = ""
            printf "Music Disabled"
        End If
        If Settings.Developer Then
            .cmdDebug.Visible = True
            printf "Developer Mode Enabled"
        Else
            .cmdDebug.Visible = False
            printf "Developer Mode Disabled"
        End If
         If Settings.Mobile Then
            .cmdMobile.Visible = True
            printf "Mobile Communication Mode Enabled"
         Else
            .cmdMobile.Visible = False
            printf "Mobile Communication Mode Disabled"
        End If
    End With
    printf "End New Settings. Settings Saved."
End Sub

'Cancel button click
Private Sub cmdCancel_Click()
    Unload Me
End Sub

'Music file chooser button_click
Private Sub cmdChooseFileMusic_Click()
CommonDialog.Filter = "Music (*.mp3)|*.mp3|"
CommonDialog.DefaultExt = "txt"
CommonDialog.DialogTitle = "Select File"
CommonDialog.ShowOpen
txtFilePath.Text = CommonDialog.FileName
End Sub

'Reset settings button_click
Private Sub cmdClearSettings_Click()
    Dim intResponse As Integer
    intResponse = MsgBox("Are you sure you want to reset all settings?", vbYesNo + vbQuestion, "Exit")
    If intResponse = vbYes Then
        Settings.clearSettings
        Settings.getSettings
        MsgBox "All settings reset" & vbNewLine & "restarting application..."
        Call Shell(App.Path & "\" & App.EXEName & ".exe", 1)
        End
    End If
End Sub

'Okay button_click
Private Sub cmdOK_Click()
    Settings.saveSettings
    Unload Me
End Sub

'Form Load
Private Sub Form_Load()
    MakeTopMost Me.hwnd
    If Settings.Backmusic Then
        optBackMusicEnable.Value = True
        optBackMusicDisable.Value = False
        If Settings.backMusicFile = Application.BackgroundMusic Then
            optMusicDefault.Value = True
            optMusicCustom.Value = False
        Else
            optMusicDefault.Value = False
            optMusicCustom.Value = True
            txtFilePath.Text = Settings.backMusicFile
        End If
    Else
        optBackMusicEnable.Value = False
        optBackMusicDisable.Value = True
    End If
    If Settings.Developer Then
        optDeveloperEnable.Value = True
        optDeveloperDisable.Value = False
    Else
        optDeveloperEnable.Value = False
        optDeveloperDisable.Value = True
    End If
    If Settings.Mobile Then
        optMobileEnable.Value = True
        optMobileDisable.Value = False
    Else
        optMobileEnable.Value = False
        optMobileDisable.Value = True
    End If
End Sub

'Disable Background Music
Private Sub optBackMusicDisable_Click()
    optMusicDefault.Enabled = False
    optMusicCustom.Enabled = False
    txtFilePath.Enabled = False
    cmdChooseFileMusic.Enabled = False
End Sub

'Enable Background Music
Private Sub optBackMusicEnable_Click()
    optMusicDefault.Enabled = True
    optMusicCustom.Enabled = True
End Sub

'Custom Music Button
Private Sub optMusicCustom_Click()
    txtFilePath.Enabled = True
    cmdChooseFileMusic.Enabled = True
End Sub

'Default Music Button
Private Sub optMusicDefault_Click()
    txtFilePath.Enabled = False
    cmdChooseFileMusic.Enabled = False
End Sub
