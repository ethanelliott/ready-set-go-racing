VERSION 5.00
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.OCX"
Begin VB.Form frmMobile 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mobile Settings"
   ClientHeight    =   4560
   ClientLeft      =   10350
   ClientTop       =   6750
   ClientWidth     =   4890
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   4890
   StartUpPosition =   2  'CenterScreen
   Begin InetCtlsObjects.Inet iNetMobile 
      Left            =   2040
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.CheckBox chkMobilePlayer2 
      Caption         =   "Player 2"
      Height          =   1215
      Left            =   3240
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3240
      Width           =   1575
   End
   Begin VB.CheckBox chkMobilePlayer1 
      Caption         =   "Player 1"
      Height          =   1215
      Left            =   1680
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Timer tmrGetStats 
      Interval        =   1
      Left            =   120
      Top             =   480
   End
   Begin VB.TextBox txtMobileCode 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1680
      TabIndex        =   0
      Top             =   2280
      Width           =   3135
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Allow Controls for:"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   0
      TabIndex        =   7
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Game Code #:"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   4
      Top             =   2280
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Network Status:"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label lblMobileTitle 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Ready S.E.T. Go Mobile Racing!"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4935
   End
   Begin VB.Label lblMobileStatus 
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1680
      TabIndex        =   1
      Top             =   1320
      Width           =   3135
   End
End
Attribute VB_Name = "frmMobile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rndNum As Long  'Random number

'Enable Mobile control for player 1 checkbox
Private Sub chkMobilePlayer1_Click()
    If chkMobilePlayer1.Value > 0 Then
        Player1.mobileControl = True
        frmMain.txtPlayer1Laps.Text = frmMain.txtPlayer1Laps.Text & "Mobile Control Enabled" & vbNewLine
    Else
        Player1.mobileControl = False
        frmMain.txtPlayer1Laps.Text = frmMain.txtPlayer1Laps.Text & "Mobile Control Disabled" & vbNewLine
    End If
    If chkMobilePlayer2.Value > 0 Then
        Player2.mobileControl = True
        frmMain.txtPlayer2Laps.Text = frmMain.txtPlayer2Laps.Text & "Mobile Control Enabled" & vbNewLine
    Else
        Player2.mobileControl = False
        frmMain.txtPlayer2Laps.Text = frmMain.txtPlayer2Laps.Text & "Mobile Control Disabled" & vbNewLine
    End If
End Sub

'Enable Mobile control for player 2 checkbox
Private Sub chkMobilePlayer2_Click()
    If chkMobilePlayer1.Value > 0 Then
        Player1.mobileControl = True
        frmMain.txtPlayer1Laps.Text = frmMain.txtPlayer1Laps.Text & "Mobile Control Enabled" & vbNewLine
    Else
        Player1.mobileControl = False
        frmMain.txtPlayer1Laps.Text = frmMain.txtPlayer1Laps.Text & "Mobile Control Disabled" & vbNewLine
    End If
    If chkMobilePlayer2.Value > 0 Then
        Player2.mobileControl = True
        frmMain.txtPlayer2Laps.Text = frmMain.txtPlayer2Laps.Text & "Mobile Control Enabled" & vbNewLine
    Else
        Player2.mobileControl = False
        frmMain.txtPlayer2Laps.Text = frmMain.txtPlayer2Laps.Text & "Mobile Control Disabled" & vbNewLine
    End If
End Sub

'Form Load
Private Sub Form_Load()
    Dim statData As String
    MakeTopMost Me.hwnd
    statData = iNetMobile.OpenURL("http://rsetg.ethanelliott.ca/?code=" & Settings.mobileCode)
    txtMobileCode.Text = Settings.mobileCode
End Sub

'Get mobile control data
Private Sub tmrGetStats_Timer()
    Dim rawdata As String
    Dim Data() As String
    If Not iNetMobile.StillExecuting Then
        rawdata = iNetMobile.OpenURL("http://rsetg.ethanelliott.ca/" & Settings.mobileCode & ".txt")
        Data() = Split(rawdata, "[*|*]")
        If UBound(Data()) > 0 Then
            If Data(0) = "Ready!" Then
                lblMobileStatus.Caption = rawdata
                If race Then
                    If Player1.mobileControl Then
                        If UBound(Data()) >= 1 And Not Data(1) = "" Then
                            Player1Drive (CInt(Data(1)))
                        End If
                    End If
                    If Player2.mobileControl Then
                        If UBound(Data()) >= 2 And Not Data(2) = "" Then
                            Player2Drive (CInt(Data(2)))
                        End If
                    End If
                End If
            Else
                lblMobileStatus.Caption = "No Network Connection"
            End If
        Else
            lblMobileStatus.Caption = "No Network Connection"
        End If
    End If
End Sub
