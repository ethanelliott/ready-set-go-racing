VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmStartScreen 
   BorderStyle     =   0  'None
   Caption         =   "Ready SET Go Racing!"
   ClientHeight    =   6375
   ClientLeft      =   4185
   ClientTop       =   2730
   ClientWidth     =   12765
   FillStyle       =   0  'Solid
   Icon            =   "frmStartScreen.frx":0000
   LinkTopic       =   "Form1"
   Picture         =   "frmStartScreen.frx":0442
   ScaleHeight     =   6375
   ScaleWidth      =   12765
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrLoading 
      Interval        =   100
      Left            =   1080
      Top             =   4200
   End
   Begin MSComctlLib.ProgressBar prgLoadingBar 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   5880
      Width           =   12765
      _ExtentX        =   22516
      _ExtentY        =   873
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Label lblLoading 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Loading..."
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   39.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1005
      Left            =   0
      TabIndex        =   2
      Top             =   4920
      Width           =   12735
   End
   Begin VB.Label lblStartLogo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Ready S.E.T. Go!"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   54
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1245
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12735
   End
End
Attribute VB_Name = "frmStartScreen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intRandNum As Integer

'Form Load
Private Sub Form_Load()
Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
SetupApplication
printf "Form Opened"
End Sub

'Loading Bar
Private Sub tmrLoading_Timer()
intRandNum = CInt(Int((10 * Rnd()) + 1))
If prgLoadingBar.Value < 90 Then
    prgLoadingBar.Value = prgLoadingBar.Value + intRandNum
Else
    prgLoadingBar.Value = 100
    tmrLoading.Enabled = False
    Sleep 1000
    frmStartScreen.Visible = False
    frmMain.Visible = True
End If
End Sub


