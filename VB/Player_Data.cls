VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Player_Data"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Name As String                           'Player's Name
Public Lap As Integer                           'Current Lap
Private lapTimes() As String                    'Array of Player's lap times
Private timeLapArr(0 To 2, 0 To 1) As Integer   'Current time array
Public CurrLapTime As String                    'Current lap time as string with ":"
Public LapListOutput As String                  'String for output to lap time list on frmMain
Public race As Boolean                          'Bool for whether the race is active or not
Public lapAverage As Long                       'Averaged lap value
Public mobileControl As Boolean                 'Bool for whether or not mobile control is active
Public Speed As Integer                         'Player's car speed

Public Sub Reset(ByVal TotLaps As Integer)
    Dim a As Integer
    Dim b As Integer
    race = False
    For a = 0 To 2 Step 1
        For b = 0 To 1 Step 1
            timeLapArr(a, b) = 0
        Next
    Next
    Name = ""
    Lap = 1
    ReDim lapTimes(TotLaps)
    LapListOutput = ""
    CurrLapTime = ""
End Sub

Public Sub NextLap()
    Dim a As Integer
    Dim b As Integer
    Dim z As Integer
    Dim sumLaps As Long
    sumLaps = 0
    If Lap < totalLaps Then
        lapTimes(Lap - 1) = CurrLapTime
        LapListOutput = LapListOutput & "Lap " & Lap & " - " & CurrLapTime & vbNewLine
        printf Name & " has finished lap #" & Lap & " in " & CurrLapTime
        For z = 0 To (Lap - 1) Step 1
            sumLaps = sumLaps + timeToLng(lapTimes(z))
        Next
        lapAverage = sumLaps / Lap
        Lap = Lap + 1
        For a = 0 To 2 Step 1
            For b = 0 To 1 Step 1
                timeLapArr(a, b) = 0
            Next
        Next
        race = True
    ElseIf Lap = totalLaps Then
        lapTimes(Lap - 1) = CurrLapTime
        LapListOutput = LapListOutput & "Lap " & Lap & " - " & CurrLapTime & vbNewLine
        printf Name & " has finished all laps!"
        race = False
    End If
End Sub

Public Function timerTick()
If timeLapArr(0, 0) = 9 Then
    timeLapArr(0, 0) = 0
    If timeLapArr(0, 1) = 5 Then
        timeLapArr(0, 1) = 0
        If timeLapArr(1, 0) = 9 Then
            timeLapArr(1, 0) = 0
            If timeLapArr(1, 1) = 5 Then
                timeLapArr(1, 1) = 0
                If timeLapArr(2, 0) = 9 Then
                    timeLapArr(2, 0) = 0
                    If timeLapArr(2, 1) = 5 Then
                        timeLapArr(2, 1) = 0
                    Else
                        timeLapArr(2, 1) = timeLapArr(2, 1) + 1
                    End If
                Else
                    timeLapArr(2, 0) = timeLapArr(2, 0) + 1
                End If
            Else
                timeLapArr(1, 1) = timeLapArr(1, 1) + 1
            End If
        Else
            timeLapArr(1, 0) = timeLapArr(1, 0) + 1
        End If
    Else
        timeLapArr(0, 1) = timeLapArr(0, 1) + 1
    End If
Else
    timeLapArr(0, 0) = timeLapArr(0, 0) + 1
End If
CurrLapTime = timeLapArr(2, 1) & timeLapArr(2, 0) & ":" & timeLapArr(1, 1) & timeLapArr(1, 0) & ":" & timeLapArr(0, 1) & timeLapArr(0, 0)
timerTick = timeLapArr(2, 1) & timeLapArr(2, 0) & ":" & timeLapArr(1, 1) & timeLapArr(1, 0) & ":" & timeLapArr(0, 1) & timeLapArr(0, 0)
End Function

Public Function getLapTime(ByVal lapNum As Long) As String
    getLapTime = lapTimes(lapNum)
End Function

Public Sub setLapTime(ByVal lapNum As Long, ByVal lapTime As String)
    lapTimes(lapNum) = lapTime
End Sub
