VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Game_Settings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Const APPLICATION_NAME As String = "rsetg"
Private Const SECTION_NAME As String = "settings"
Private Const KEY_BACKMUSIC As String = "backMusic"
Private Const KEY_DEVELOPER As String = "developer"
Private Const KEY_BACKMUSIC_FILE As String = "backmusicfile"
Private Const KEY_MOBILE As String = "mobileControl"
Private Const KEY_MOBILE_KEY As String = "mobilekey"
Private Const KEY_EULA As String = "eula"
Public Eula As Boolean
Public Backmusic As Boolean
Public Developer As Boolean
Public Mobile As Boolean
Public backMusicFile As String
Public mobileKey As Long
Private x As Long

'Save Settings
Public Sub saveSettings()
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_BACKMUSIC, Backmusic
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_DEVELOPER, Developer
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_BACKMUSIC_FILE, backMusicFile
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_MOBILE, Mobile
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_MOBILE_KEY, mobileKey
    SaveSetting APPLICATION_NAME, SECTION_NAME, KEY_EULA, Eula
End Sub

'Get Settings
Public Sub getSettings()
    x = CStr(Format((100 * Rnd() * Rnd() * Rnd()), "00") & "" & Format((100 * Rnd() * Rnd() * Rnd()), "00") & "" & Format((100 * Rnd() * Rnd() * Rnd()), "00"))
    Backmusic = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_BACKMUSIC, True)
    Developer = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_DEVELOPER, False)
    Mobile = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_MOBILE, False)
    mobileCode = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_MOBILE_KEY, x)
    backMusicFile = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_BACKMUSIC_FILE, (App.Path & "\backgroundMusic.mp3"))
    Eula = GetSetting(APPLICATION_NAME, SECTION_NAME, KEY_EULA, True)
End Sub

'Erase all settings
Public Sub clearSettings()
    DeleteSetting APPLICATION_NAME, SECTION_NAME
End Sub
