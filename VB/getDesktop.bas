Attribute VB_Name = "getWindowsPath"
'===============================================================
'   Windows environment directory variable function
'   Code (C) www.VB6.US
'   Author: Unknown
'==============================================================
Public Const CSIDL_DESKTOP = &H0   ' Desktop (namespace root)
Public Const CSIDL_INTERNET = &H1  ' Internet virtual folder
Public Const CSIDL_PROGRAMS = &H2  ' Programs folder (under Start menu in [user] profile)
Public Const CSIDL_CONTROLS = &H3  ' Control Panel virtual folder
Public Const CSIDL_PRINTERS = &H4  ' Printers virtual folder
Public Const CSIDL_PERSONAL = &H5  ' Personal folder ([user] profile)
Public Const CSIDL_FAVORITES = &H6 ' Favorites folder ([user] profile)
Public Const CSIDL_STARTUP = &H7   ' Startup folder ([user] profile)
Public Const CSIDL_RECENT = &H8    ' Recent Documents folder ([user] profile)
Public Const CSIDL_SENDTO = &H9    ' SendTo folder ([user] profile)
Public Const CSIDL_DESKTOPDIRECTORY = &H10 ' Desktop folder ([user] profile)
Public Const CSIDL_DRIVES = &H11   ' My Computer virtual folder
Public Const CSIDL_NETWORK = &H12  ' Network Neighborhood root
Public Const CSIDL_NETHOOD = &H13  ' Network Neighborhood directory
Public Const CSIDL_FONTS = &H14    ' Fonts virtual folder
Public Const CSIDL_TEMPLATES = &H15 ' Templates folder ([user] profile)
Public Const CSIDL_COMMON_STARTMENU = &H16 ' Start menu (All Users profile)
Public Const CSIDL_COMMON_PROGRAMS = &H17 ' Programs folder (under Start menu in All Users profile)
Public Const CSIDL_COMMON_STARTUP = &H18 ' Startup folder (All Users profile)
Public Const CSIDL_COMMON_DESKTOPDIRECTORY = &H19 ' Desktop folder (All Users profile)
Public Const CSIDL_INTERNET_CACHE = &H20    ' Internet Cache folder (Explorer 4.01 and Windows� 98).
Public Const CSIDL_COOKIES = &H21 ' Cookies folder
Public Const CSIDL_HISTORY = &H22 ' History folder
Public Const CSIDL_BITBUCKET = &HA  ' Recycle Bin folder
Public Const CSIDL_STARTMENU = &HB  ' Start menu ([user] profile)
Public Const CSIDL_APPDATA = &H1A ' Application Data ([user] profile) (Internet Explorer 4.0).
Public Const CSIDL_ALTSTARTUP = &H1D ' Alternate Startup ([user], DBCS)
Public Const CSIDL_COMMON_ALTSTARTUP = &H1E ' Alternate Startup folder (All Users profile, DBCS)
Public Const CSIDL_COMMON_FAVORITES = &H1F  ' Favorites folder (All Users profile)
Public Const CSIDL_PRINTHOOD = &H1B ' PrintHood folder ([user] profile)
Public Const CSIDL_MYPICTURES = &H27 ' My Pictures folder (Windows 2000 & Windows Me).
Public Const CSIDL_COMMON_ADMINTOOLS = &H2F ' Administrative tools (All Users profile) (Windows 2000 & Windows Me).
Public Const CSIDL_COMMON_DOCUMENTS = &H2E ' Documents folder (All Users profile)
Public Const CSIDL_ADMINTOOLS = &H30  ' Administrative Tools ([user] profile) (Windows 2000 & Windows Me).
Public Const CSIDL_PROGRAM_FILES = &H26     ' Program Files folder (Windows 2000 & Windows Me).
Public Const CSIDL_PROGRAM_FILES_COMMON = &H2B    ' Common Files folder  (Windows 2000 & Windows Me).
Public Const CSIDL_COMMON_APPDATA = &H23 ' Application data for all users. A typical path is C:\Documents and Settings\All Users\Application (Windows 2000 & Windows Me)
Public Const CSIDL_COMMON_TEMPLATES = &H2D ' File system directory that contains the templates that are available to all users. A typical path is C:\Documents and Settings\All Users\Templates. Valid only for Windows NT systems.
Public Const CSIDL_CONNECTIONS = &H31 ' Virtual folder containing Network and Dial-up connetions
Public Const CSIDL_LOCAL_APPDATA = &H1C ' File system directory that serves as a data repository for local (nonroaming) applications. A typical path is C:\Documents and Settings\username\Local Settings\Application Data (Windows 2000 & Windows Me).
Public Const CSIDL_PROFILE = &H28 'User's profile folder (Windows 2000 & Windows Me).
Public Const CSIDL_PROGRAM_FILES_COMMONX86 = &H2C ' The x86 Program Files Common folder on RISC systems.
Public Const CSIDL_PROGRAM_FILESX86 = &H2A ' The x86 Program Files folder on RISC systems.
Public Const CSIDL_SYSTEM = &H25 ' System folder. A typical path is C:\WINNT\SYSTEM32 (Windows 2000 & Windows Me).
Public Const CSIDL_SYSTEMX86 = &H29 ' The x86 system directory on RISC systems.
Public Const CSIDL_WINDOWS = &H24 ' Windows directory or SYSROOT. This corresponds to the %windir% or %SYSTEMROOT% environment variables. A typical path is C:\WINNT (Windows 2000 & Windows Me).
Public Const CSIDL_MYMUSIC = &HD ' File system directory that serves as a common repository for music files.
Public Const CSIDL_MYVIDEO = &HE ' File system directory that serves as a common repository for video files.
Public Const CSIDL_COMMON_MUSIC = &H35 ' My Music folder for all users. See CSIDL_MYMUSIC.
Public Const CSIDL_COMMON_PICTURES = &H36 ' My Pictures folder for all users. See CSIDL_MYPICTURES.
Public Const CSIDL_COMMON_VIDEO = &H37 ' My Video folder for all users. See CSIDL_MYVIDEO.
Public Const CSIDL_RESOURCES = &H38 ' System resource directory. A typical path is C:\WINNT\Resources.
Public Const CSIDL_RESOURCES_LOCALIZED = &H39 ' Localized resource directory. See CSIDL_RESOURCES.
Public Const CSIDL_COMMON_OEM_LINKS = &H3A  ' Folder containing links to All Users OEM specific applications.
Public Const CSIDL_CDBURN_AREA = &H3B ' File system folder used to hold data for burning to a CD. Typically [User Profile Folder]\Local Settings\Applications Data\Microsoft\CD Burning.
Public Const CSIDL_COMPUTERSNEARME = &H3D    ' Computers Near Me folder. Virtual folder containing links to "nearby" computers on the network. Nearness it is established by common workgroup membership.

Private Declare Function SHGetFolderPath Lib "shfolder" Alias "SHGetFolderPathA" _
(ByVal hwndOwner As Long, ByVal nFolder As Long, ByVal hToken As Long, _
ByVal dwFlags As Long, ByVal pszPath As String) As Long

'Get Path Function
Public Function GetPath(ByVal Wlocation As Long) As String
    Dim strBuffer  As String
    Dim strPath    As String
    Dim lngReturn  As Long
    Dim lngCSIDL   As Long
    lngCSIDL = Wlocation
    strPath = String(260, 0)
    lngReturn = SHGetFolderPath(0, lngCSIDL, 0, &H0, strPath)
    GetPath = Left$(strPath, InStr(1, strPath, Chr(0)) - 1)
End Function

