VERSION 5.00
Begin VB.Form frmDebug 
   BackColor       =   &H00000000&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Debug"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12480
   Icon            =   "frmDebug.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   12480
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00000000&
      Caption         =   "INP/OUT/VAR"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   855
      Left            =   120
      TabIndex        =   3
      Top             =   7320
      Width           =   12255
      Begin VB.Timer tmrUpdateVal 
         Interval        =   1
         Left            =   0
         Top             =   360
      End
      Begin VB.Label lblDebugRace 
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "FALSE"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   10440
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackColor       =   &H00000000&
         Caption         =   "Race:"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   9600
         TabIndex        =   9
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblDebugInput 
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "00000000"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   7080
         TabIndex        =   8
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label4 
         BackColor       =   &H00000000&
         Caption         =   "Current Input:"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   5040
         TabIndex        =   7
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblDebugOutput 
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "00000000"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   2520
         TabIndex        =   6
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Current Output:"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   375
         Left            =   360
         TabIndex        =   4
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.TextBox txtDebugIn 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Consolas"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   495
      Left            =   360
      TabIndex        =   0
      Top             =   6840
      Width           =   12135
   End
   Begin VB.TextBox txtDebugOut 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Consolas"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   6855
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   0
      Width           =   12375
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Current Output:"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   375
      Left            =   2400
      TabIndex        =   5
      Top             =   7560
      Width           =   2055
   End
   Begin VB.Label lblDebugInArrow 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "Consolas"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   6840
      Width           =   375
   End
End
Attribute VB_Name = "frmDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Form Load
Private Sub Form_Load()
    MakeTopMost Me.hwnd
    txtDebugOut.Text = strDebugSaveData
End Sub

'Timer to update debug values
Private Sub tmrUpdateVal_Timer()
lblDebugOutput.Caption = SETOut
lblDebugInput.Caption = inVal
lblDebugRace.Caption = race
End Sub

'Keypress inside of text input
Private Sub txtDebugIn_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then       'Enter Key
        enterCommand txtDebugIn.Text
    End If
End Sub

'Enter Command Interpretter function
Private Sub enterCommand(ByVal command As String)
    Dim x As String
    txtDebugIn.Text = ""
    command = UCase(command)
    printf "> " & command, False
    If command = UCase("test") Then
        printf "Hello World"
    ElseIf command = UCase("help") Then
        printf "Help Menu", False
        printf "Help....................Display this menu", False
        printf "Test....................print 'Hello World'", False
        printf "Exit....................Exit the application", False
        printf "Output..................Set the value of the out command", False
        printf "Input...................prints the current input value (int)", False
        printf "startRace...............starts a race", False
        printf "stopRace................Stops the current race", False
        printf "resetRace...............Reset the race", False
        printf "nextLap.................Sets a specified car to the next lap", False
    ElseIf command = UCase("exit") Then
        printf "Exiting Application"
        CloseApp
    ElseIf InStr(1, command, UCase("output")) > 0 Then
        x = Mid(command, 8)
        printf "Outputting: " & x & " to the port"
    ElseIf command = UCase("input") Then
        printf "Current Input: 0"
    ElseIf command = UCase("stoprace") Then
        stopRace
    ElseIf InStr(1, command, UCase("startrace")) > 0 Then
        Dim y() As String
        Dim u As String
        Dim t() As String
        y() = Split(command, " ", 2)
        If (UBound(y) + 1) > 1 Then
            u = y(1)
            t() = Split(u, ",", 3)
            If (UBound(t) + 1) = 3 Then
                If Not IsNull(t(0)) And Not IsNull(t(1)) And Not IsNull(t(2)) Then
                    resetRace
                    loadRace t(0), t(1), t(2)
                    startRace
                End If
            Else
                printf "Too Few Arguments for command"
            End If
        Else
            printf "Too Few Arguments for command"
        End If
    ElseIf InStr(1, command, UCase("nextlap")) > 0 Then
        x = Mid(command, 9)
        If x = "1" Then
            Player1.NextLap
        ElseIf x = "2" Then
            Player2.NextLap
        Else
            printf "Invalid Player Number!"
        End If
    Else
        printf "' " & command & " ' is not a valid command."
    End If
    txtDebugOut.SelStart = Len(txtDebugOut.Text)
End Sub
