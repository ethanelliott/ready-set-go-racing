VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "application_data"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Version As String                 'Application Version
Public Release As String                 'Application Release Date
Public Developers As String              'Developers of the application
Public Description As String             'Short description of the application
Public BackgroundMusic As String
Public beeps As String

'Load Variables
Public Sub loadVariables()
    Version = "0.0.1"
    Release = "15/01/15"
    Developers = "Ethan, Sumeet, Tyler"
    Description = "Get ready, get S.E.T. for the most immersive driving simulator EVER."
    BackgroundMusic = App.Path & "\backgroundMusic.mp3"
    beeps = App.Path & "\countdownbeeps.mp3"
End Sub
