VERSION 5.00
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.OCX"
Begin VB.Form frmEULA 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "EULA"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   9270
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   9270
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdDissagree 
      Caption         =   "Disagree"
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   6000
      Width           =   3495
   End
   Begin VB.CommandButton cmdAgree 
      Caption         =   "Agree"
      Height          =   495
      Left            =   5760
      TabIndex        =   1
      Top             =   6000
      Width           =   3495
   End
   Begin InetCtlsObjects.Inet Inet1 
      Left            =   4320
      Top             =   6000
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.TextBox txtEula 
      BeginProperty Font 
         Name            =   "Consolas"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5895
      Left            =   0
      MouseIcon       =   "frmEULA.frx":0000
      MousePointer    =   99  'Custom
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   0
      Width           =   9255
   End
End
Attribute VB_Name = "frmEULA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Agree button click
Private Sub cmdAgree_Click()
    Settings.Eula = False
    Settings.saveSettings
    Me.Visible = False
End Sub

'Disagree button click
Private Sub cmdDissagree_Click()
    Settings.Eula = True
    Settings.saveSettings
    CloseApp
End Sub

'Form Load
Private Sub Form_Load()
    Dim Data As String
    Dim DataLines() As String
    Dim Index As Long
    Dim outPut As String
    MakeTopMost Me.hwnd
    Data = Inet1.OpenURL("http://rsetg.ethanelliott.ca/EULA.txt")
    DataLines = Split(Data, "|*|")
    For Index = LBound(DataLines) To UBound(DataLines)
      outPut = outPut & DataLines(Index) & vbNewLine
    Next
    txtEula.Text = outPut
End Sub
