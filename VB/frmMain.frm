VERSION 5.00
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.OCX"
Begin VB.Form frmMain 
   BackColor       =   &H8000000D&
   BorderStyle     =   0  'None
   Caption         =   "Ready S.E.T. Go Racing"
   ClientHeight    =   9210
   ClientLeft      =   4470
   ClientTop       =   435
   ClientWidth     =   9870
   ForeColor       =   &H00FFFFFF&
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   9210
   ScaleWidth      =   9870
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrFlashingLights 
      Interval        =   1000
      Left            =   360
      Top             =   840
   End
   Begin VB.Timer tmrCountDown 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   5640
      Top             =   3480
   End
   Begin InetCtlsObjects.Inet iNet 
      Left            =   7080
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.CommandButton cmdMobile 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   2880
      Picture         =   "frmMain.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   0
      Width           =   615
   End
   Begin VB.ComboBox cmbLapNum 
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      ItemData        =   "frmMain.frx":0884
      Left            =   3240
      List            =   "frmMain.frx":08C4
      TabIndex        =   23
      Text            =   "# of Laps"
      Top             =   5880
      Width           =   1695
   End
   Begin VB.Timer tmrPlayer2 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   8160
      Top             =   2160
   End
   Begin VB.Timer tmrPlayer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   720
      Top             =   1920
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5880
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSettings 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   2160
      Picture         =   "frmMain.frx":090F
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   0
      Width           =   615
   End
   Begin VB.CommandButton cmdDebug 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   3600
      Picture         =   "frmMain.frx":0D51
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   0
      Width           =   615
   End
   Begin VB.Timer tmrStopwatch 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   4440
      Top             =   2160
   End
   Begin VB.CommandButton cmdScores 
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   1440
      Picture         =   "frmMain.frx":1193
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   0
      Width           =   615
   End
   Begin VB.Timer tmrUpdate 
      Interval        =   1
      Left            =   4080
      Top             =   360
   End
   Begin VB.CommandButton cmdStartRace 
      Caption         =   "Start Race"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5160
      TabIndex        =   10
      Top             =   5880
      Width           =   1455
   End
   Begin VB.TextBox txtPlayer2 
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3240
      MaxLength       =   10
      TabIndex        =   9
      Text            =   "Player 2"
      Top             =   5280
      Width           =   3375
   End
   Begin VB.TextBox txtPlayer1 
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3240
      MaxLength       =   10
      TabIndex        =   8
      Text            =   "Player 1"
      Top             =   4680
      Width           =   3375
   End
   Begin VB.PictureBox SelectBox 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      FontTransparent =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   0
      ScaleHeight     =   2535
      ScaleWidth      =   9855
      TabIndex        =   0
      Top             =   6720
      Width           =   9855
   End
   Begin VB.CommandButton cmdAbout 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   720
      MaskColor       =   &H00000000&
      Picture         =   "frmMain.frx":15D5
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   615
   End
   Begin VB.CommandButton cmdHelp 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Height          =   615
      Left            =   0
      MaskColor       =   &H00000000&
      Picture         =   "frmMain.frx":1D17
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   615
   End
   Begin VB.CommandButton cmdExit 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      Height          =   615
      Left            =   9240
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmMain.frx":2459
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox txtPlayer1Laps 
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1215
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   14
      Top             =   5160
      Width           =   2895
   End
   Begin VB.TextBox txtPlayer2Laps 
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1215
      Left            =   6960
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   15
      Top             =   5160
      Width           =   2895
   End
   Begin WMPLibCtl.WindowsMediaPlayer wmpCountdownBeep 
      Height          =   495
      Left            =   3960
      TabIndex        =   25
      Top             =   3840
      Visible         =   0   'False
      Width           =   975
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   1720
      _cy             =   873
   End
   Begin VB.Image imgStopRace 
      Height          =   1920
      Left            =   3840
      Picture         =   "frmMain.frx":289B
      Stretch         =   -1  'True
      ToolTipText     =   "Press the space bar"
      Top             =   2760
      Visible         =   0   'False
      Width           =   1920
   End
   Begin VB.Label lblStopWatch2 
      Caption         =   "Label3"
      Height          =   255
      Left            =   5880
      TabIndex        =   22
      Top             =   1680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblStopWatch1 
      Caption         =   "Label3"
      Height          =   255
      Left            =   3120
      TabIndex        =   21
      Top             =   1680
      Visible         =   0   'False
      Width           =   855
   End
   Begin WMPLibCtl.WindowsMediaPlayer wmpBackground 
      Height          =   495
      Left            =   4920
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   240
      Visible         =   0   'False
      Width           =   855
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   -1  'True
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   1508
      _cy             =   873
   End
   Begin VB.Label lblLapNumP2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Lap: 0 / 0 Average: 00:00:00"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   660
      Left            =   4725
      TabIndex        =   17
      Top             =   4560
      Width           =   7440
   End
   Begin VB.Label lblLapNumP1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Lap: 0 / 0 Average: 00:00:00"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   660
      Left            =   -2355
      TabIndex        =   16
      Top             =   4560
      Width           =   7440
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   20
      X1              =   0
      X2              =   9840
      Y1              =   6600
      Y2              =   6600
   End
   Begin VB.Label lblPlayer2Name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6960
      TabIndex        =   12
      Top             =   1680
      Width           =   2895
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPlayer1Name 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   0
      TabIndex        =   11
      Top             =   1680
      Width           =   2895
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblStopWatch 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "00:00.00"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1155
      Left            =   3030
      TabIndex        =   7
      Top             =   1800
      Width           =   3795
   End
   Begin VB.Line Line4 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   10
      X1              =   6840
      X2              =   6840
      Y1              =   1560
      Y2              =   6480
   End
   Begin VB.Image imgCount3 
      Height          =   4095
      Left            =   3000
      Picture         =   "frmMain.frx":2CDD
      Stretch         =   -1  'True
      Top             =   6480
      Visible         =   0   'False
      Width           =   4170
   End
   Begin VB.Image imgCount1 
      Height          =   4095
      Left            =   3360
      Picture         =   "frmMain.frx":2FE7
      Stretch         =   -1  'True
      Top             =   6360
      Visible         =   0   'False
      Width           =   4170
   End
   Begin VB.Image imgCount2 
      Height          =   4095
      Left            =   3240
      Picture         =   "frmMain.frx":32F1
      Stretch         =   -1  'True
      Top             =   6000
      Visible         =   0   'False
      Width           =   4170
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   10
      X1              =   3000
      X2              =   3000
      Y1              =   1560
      Y2              =   6600
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   10
      X1              =   0
      X2              =   9840
      Y1              =   1560
      Y2              =   1560
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Ready S.E.T. Go Racing!"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   975
      Left            =   0
      TabIndex        =   3
      Top             =   360
      Width           =   9855
   End
   Begin VB.Label label1 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   1800
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Height          =   495
      Left            =   7080
      TabIndex        =   2
      Top             =   1920
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Image imgPlayer1Speed 
      Height          =   2520
      Index           =   2
      Left            =   360
      Picture         =   "frmMain.frx":35FB
      Stretch         =   -1  'True
      Top             =   2520
      Width           =   2280
   End
   Begin VB.Image imgPlayer1Speed 
      Height          =   2520
      Index           =   1
      Left            =   360
      Picture         =   "frmMain.frx":3A3D
      Stretch         =   -1  'True
      Top             =   2520
      Width           =   2280
   End
   Begin VB.Image imgPlayer2Speed 
      Height          =   2895
      Index           =   2
      Left            =   7200
      Picture         =   "frmMain.frx":3E7F
      Stretch         =   -1  'True
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Image imgPlayer2Speed 
      Height          =   2895
      Index           =   1
      Left            =   7080
      Picture         =   "frmMain.frx":42C1
      Stretch         =   -1  'True
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Image imgPlayer2Speed 
      Height          =   2895
      Index           =   0
      Left            =   7080
      Picture         =   "frmMain.frx":4703
      Stretch         =   -1  'True
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Image imgPlayer1Speed 
      Height          =   2520
      Index           =   0
      Left            =   360
      Picture         =   "frmMain.frx":4B45
      Stretch         =   -1  'True
      Top             =   2520
      Width           =   2280
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim i As Long       'variable for index values in "for" loops
Dim s As String     'variable used to send strings to output

'About Button_Click
Private Sub cmdAbout_Click()
    printf "About Button Pressed"
    frmAbout.Visible = True
End Sub

'Debug Button_Click
Private Sub cmdDebug_Click()
    frmDebug.Visible = True
    printf "Debug form opened"
End Sub

'Form Exit Button_Click
Private Sub cmdExit_Click()
    printf "Exit Button Pressed"
    CloseApp
End Sub

'Mobile Racing Control Button_Click
Private Sub cmdMobile_Click()
    frmMobile.Visible = True
    printf "Mobile form Opened"
End Sub

'View Scores Button_Click
Private Sub cmdScores_Click()
    printf "Highscore Button Pressed"
    Highscore.LoadScores 9, 0, "NULL", "rsetg", False
    s = "Highscores:" & vbCrLf & vbCrLf
    For i = 0 To 9
        s = s & Highscore.GetPlayerName(i) & " - " & Highscore.GetPlayerScore(i) & vbCrLf
    Next i
    printf "Successfully retrieved highscores"
    MsgBox "========================================" & vbNewLine & s & vbNewLine & "========================================", vbOKOnly, "Highscore List"
End Sub

'Settings Button_Click
Private Sub cmdSettings_Click()
    frmOptions.Visible = True
End Sub

'Start Race Button_Click
Private Sub cmdStartRace_Click()
    printf "Start-Race Button Pressed"
    If IsNumeric(cmbLapNum.Text) Then
        If CInt(cmbLapNum.Text) <= 20 Then
            resetRace
            loadRace txtPlayer1.Text, txtPlayer2.Text, cmbLapNum.Text
            startRace
            realignForm
        Else
            printf "Error starting race - number greater than 20"
            MsgBox "The number of laps must be a number less than 20!", vbExclamation, "Error"
        End If
    Else
        printf "Error starting race - invalid character entered"
        MsgBox "The number of laps must be a number!", vbExclamation, "Error"
    End If
End Sub

'Form Load
Private Sub Form_Load()
    printf "Main Form Loaded"
    Highscore.LoadScores 9, 0, "NULL", "rsetg", False
    printf "Highscores Loaded"
    If Settings.Backmusic Then
        wmpBackground.Settings.setMode "loop", True
        wmpBackground.URL = Settings.backMusicFile
        printf "Music Enabled"
    Else
        wmpBackground.URL = ""
        printf "Music Disabled"
    End If
    race = False
    If Settings.Developer Then
        cmdDebug.Visible = True
        printf "Developer Mode Enabled"
    Else
        cmdDebug.Visible = False
        printf "Developer Mode Disabled"
    End If
    If Settings.Mobile Then
        cmdMobile.Visible = True
        printf "Mobile Communication Mode Enabled"
    Else
        cmdMobile.Visible = False
        printf "Mobile Communication Mode Disabled"
    End If
    frmEULA.Visible = Settings.Eula
End Sub

'Stop Race Image_Click
Private Sub imgStopRace_Click()
    stopRace
End Sub

'Key press event inside of the select box
Private Sub SelectBox_KeyDown(KeyCode As Integer, Shift As Integer)
    If Asc(UCase(Chr(KeyCode))) = 27 Then   'ESC
        CloseApp
    End If
    If race Then
        If Asc(UCase(Chr(KeyCode))) = 32 Then       'Space Bar
            stopRace
        End If
        If Not Player1.mobileControl Then
            If Asc(UCase(Chr(KeyCode))) = 81 Then       'Q
                label1.Caption = "FAST"
                Player1Drive (1)
            End If
        End If
        If Not Player2.mobileControl Then
            If Asc(UCase(Chr(KeyCode))) = 80 Then       'P
                Label2.Caption = "FAST"
                Player2Drive (1)
            End If
        End If
        Sleep 1
    End If
End Sub

'Key release inside of select box
Private Sub SelectBox_KeyUp(KeyCode As Integer, Shift As Integer)
    If race Then
        If Not Player1.mobileControl Then
            If Asc(UCase(Chr(KeyCode))) = 81 Then 'Q
                label1.Caption = "NO DRIVE"
                Player1Drive (0)
            End If
            If Asc(UCase(Chr(KeyCode))) = 87 Then 'W
                label1.Caption = "NO DRIVE"
                Player1Drive (0)
            End If
        End If
        If Not Player2.mobileControl Then
            If Asc(UCase(Chr(KeyCode))) = 79 Then 'O
                Label2.Caption = "NO DRIVE"
                Player2Drive (0)
            End If
            If Asc(UCase(Chr(KeyCode))) = 80 Then 'P
                Label2.Caption = "NO DRIVE"
                Player2Drive (0)
            End If
        End If
    End If
End Sub

'Resize form function
Private Sub Form_Resize()
    realignForm     'Reset the form to match the newly-resized form.
End Sub

'Countdown timer to display "3,2,1,GO"
Private Sub tmrCountDown_Timer()
    If c = 1 Then
        imgCount1.Visible = False
        imgCount2.Visible = False
        imgCount3.Visible = True
        c = c + 1
    ElseIf c = 2 Then
        imgCount1.Visible = False
        imgCount2.Visible = True
        imgCount3.Visible = False
        c = c + 1
    ElseIf c = 3 Then
        imgCount1.Visible = True
        imgCount2.Visible = False
        imgCount3.Visible = False
        c = c + 1
    ElseIf c = 4 Then
        imgCount1.Visible = False
        imgCount2.Visible = False
        imgCount3.Visible = False
        tmrCountDown.enabled = False
        wmpBackground.URL = Settings.backMusicFile
        startRaceGo
    End If
End Sub

Private Sub tmrFlashingLights_Timer()
If lights Then
    lights = False
Else
    lights = True
End If
End Sub

'Player 1 Clock
Private Sub tmrPlayer1_Timer()
    lblStopWatch1.Caption = Player1.timerTick()
    lblLapNumP1.Caption = "Lap: " & Player1.Lap & "/" & totalLaps & vbNewLine & "Average: " & lngToTime(Player1.lapAverage)
    txtPlayer1Laps.Text = Player1.LapListOutput
    tmrPlayer1.enabled = Player1.race
End Sub

'Player 2 Clock
Private Sub tmrPlayer2_Timer()
    lblStopWatch2.Caption = Player2.timerTick()
    lblLapNumP2.Caption = "Lap: " & Player2.Lap & "/" & totalLaps & vbNewLine & "Average: " & lngToTime(Player2.lapAverage)
    txtPlayer2Laps.Text = Player2.LapListOutput
    tmrPlayer2.enabled = Player2.race
End Sub

'Stopwatch timer tick
Private Sub tmrStopwatch_Timer()
If timeArr(0, 0) = 9 Then
    timeArr(0, 0) = 0
    If timeArr(0, 1) = 5 Then
        timeArr(0, 1) = 0
        If timeArr(1, 0) = 9 Then
            timeArr(1, 0) = 0
            If timeArr(1, 1) = 5 Then
                timeArr(1, 1) = 0
                If timeArr(2, 0) = 9 Then
                    timeArr(2, 0) = 0
                    If timeArr(2, 1) = 5 Then
                        timeArr(2, 1) = 0
                    Else
                        timeArr(2, 1) = timeArr(2, 1) + 1
                    End If
                Else
                    timeArr(2, 0) = timeArr(2, 0) + 1
                End If
            Else
                timeArr(1, 1) = timeArr(1, 1) + 1
            End If
        Else
            timeArr(1, 0) = timeArr(1, 0) + 1
        End If
    Else
        timeArr(0, 1) = timeArr(0, 1) + 1
    End If
Else
    timeArr(0, 0) = timeArr(0, 0) + 1
End If
timeLap = timeArr(2, 1) & timeArr(2, 0) & ":" & timeArr(1, 1) & timeArr(1, 0) & ":" & timeArr(0, 1) & timeArr(0, 0)
lblStopWatch.Caption = timeLap
End Sub

'Update race values and input / output values
Private Sub tmrUpdate_Timer()
    'Out OUTPUT_ADDRESS, SETOut()
    'inVal = Inp(INPUT_ADDRESS)
    If race Then
        SelectBox.SetFocus
        If Not Player1.race And Not Player2.race Then
            stopRace
        End If
    End If
End Sub

'Re-align form sub-routine
Public Sub realignForm()
    If Me.Width < 10125 Then
        Me.Width = 10125
    End If
    If Me.Height < 9795 Then
        Me.Height = 9795
    End If
    With SelectBox
        .Top = (Me.Height * 0.85)
        .Height = (Me.Height * 0.15)
        .Width = Me.Width
    End With
    lblTitle.Width = Me.Width
    Line1.X2 = Me.Width
    With Line2
        .X2 = Me.Width
        .X1 = 0
        .Y2 = (Me.Height * 0.85) - .BorderWidth
        .Y1 = (Me.Height * 0.85) - .BorderWidth
    End With
    With Line3
        .X2 = Me.Width * 0.33
        .X1 = Me.Width * 0.33
        .Y2 = Line2.Y1
        .Y1 = Line1.Y1
    End With
    With Line4
        .X2 = Me.Width * 0.66
        .X1 = Me.Width * 0.66
        .Y2 = Line2.Y1
        .Y1 = Line1.Y1
    End With
    lblStopWatch.Top = Line1.Y1 + (lblStopWatch.Height * 0.5)
    lblStopWatch.Left = (Me.Width / 2) - (lblStopWatch.Width / 2)
    imgCount1.Left = (Me.Width / 2) - (imgCount1.Width / 2)
    imgCount1.Top = (Me.Height / 2) - (imgCount1.Height / 2)
    imgCount2.Left = (Me.Width / 2) - (imgCount2.Width / 2)
    imgCount2.Top = (Me.Height / 2) - (imgCount2.Height / 2)
    imgCount3.Left = (Me.Width / 2) - (imgCount3.Width / 2)
    imgCount3.Top = (Me.Height / 2) - (imgCount3.Height / 2)
    imgCount1.Visible = False
    imgCount2.Visible = False
    imgCount3.Visible = False
    cmdExit.Left = Me.Width - (cmdExit.Width)
    txtPlayer1.Width = (Line4.X1 - Line3.X1) * 0.9
    txtPlayer1.Left = Line3.X1 + ((Line4.X1 - Line3.X1) * 0.05)
    txtPlayer2.Width = (Line4.X1 - Line3.X1) * 0.9
    txtPlayer2.Left = Line3.X1 + ((Line4.X1 - Line3.X1) * 0.05)
    cmbLapNum.Width = (Line4.X1 - Line3.X1) * 0.425
    cmbLapNum.Left = Line3.X1 + ((Line4.X1 - Line3.X1) * 0.05)
    cmdStartRace.Width = (Line4.X1 - Line3.X1) * 0.425
    cmdStartRace.Left = (cmbLapNum.Left + cmbLapNum.Width) + ((Line4.X1 - Line3.X1) * 0.05)
    cmbLapNum.Top = Line2.Y1 - (cmbLapNum.Height * 2)
    cmdStartRace.Top = cmbLapNum.Top
    txtPlayer2.Top = cmbLapNum.Top - (txtPlayer2.Height * 1.5)
    txtPlayer1.Top = txtPlayer2.Top - (txtPlayer1.Height * 1.5)
    lblPlayer1Name.Width = (Line3.X1 * 0.9)
    lblPlayer1Name.Left = (Line3.X1 * 0.05) - Line3.BorderWidth
    lblPlayer1Name.Top = Line1.Y1 + (Line1.BorderWidth * 10)
    lblPlayer2Name.Width = (Line3.X1 * 0.9)
    lblPlayer2Name.Left = Line4.X1 + (Line3.X1 * 0.05) - Line3.BorderWidth
    lblPlayer2Name.Top = Line1.Y1 + (Line1.BorderWidth * 10)
    For i = 0 To 2 Step 1
        With imgPlayer1Speed(i)
            .Width = (Line3.X1 * 0.5)
            .Left = (Line3.X1 * 0.25) - Line3.BorderWidth
            .Height = .Width + 240
            .Top = Line1.Y1 + (lblPlayer1Name.Height * 2)
        End With
    Next
    For i = 0 To 2 Step 1
        With imgPlayer2Speed(i)
            .Width = (Line3.X1 * 0.5)
            .Left = Line4.X1 + (Line3.X1 * 0.25) - Line3.BorderWidth
            .Height = .Width + 240
            .Top = Line1.Y1 + (lblPlayer2Name.Height * 2)
        End With
    Next
    With lblLapNumP1
        .Caption = "Lap: 0 / 0" & vbNewLine & "Average: 00:00:00"
        .Top = (imgPlayer1Speed(0).Top + imgPlayer1Speed(0).Height)
        .Left = (Line3.X1 / 2) - (.Width / 2)
    End With
    With lblLapNumP2
        .Caption = "Lap: 0 / 0" & vbNewLine & "Average: 00:00:00"
        .Top = (imgPlayer2Speed(0).Top + imgPlayer2Speed(0).Height)
        .Left = ((Me.Width + Line4.X1) / 2) - (.Width / 2)
    End With
    With txtPlayer1Laps
        .Width = (Line3.X1 * 0.9)
        .Left = (Line3.X1 * 0.05) - Line3.BorderWidth
        .Height = Line2.Y1 - (lblLapNumP1.Top + lblLapNumP1.Height + 400)
        .Top = (lblLapNumP1.Top + lblLapNumP1.Height + 200)
    End With
    With txtPlayer2Laps
        .Width = (Line3.X1 * 0.9)
        .Left = Line4.X1 + (Line3.X1 * 0.05) - Line3.BorderWidth
        .Height = Line2.Y1 - (lblLapNumP2.Top + lblLapNumP2.Height + 400)
        .Top = (lblLapNumP2.Top + lblLapNumP2.Height + 200)
    End With
    With imgStopRace
        .Width = ((Line4.X1 - Line3.X1) * 0.6)
        .Height = .Width
        .Top = txtPlayer1.Top - (txtPlayer1.Height * 2) - .Height
        .Left = (Line3.X1 * 0.2) + Line3.X1 - Line3.BorderWidth
    End With
printf "Window resized"
End Sub
