Attribute VB_Name = "moduleMain"
'=================================================================================
'                   Copyright (c) 2015 Ready S.E.T. Industries
'
'   Permission is hereby granted, free of charge, to any person obtaining a copy
'   of this software and associated documentation files (the "Software"), to deal
'   in the Software without restriction, including without limitation the rights
'   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
'   copies of the Software, and to permit persons to whom the Software is
'   furnished to do so, subject to the following conditions:
'
'   The above copyright notice and this permission notice shall be included in
'   all copies or substantial portions of the Software.
'
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
'   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
'   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
'   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
'   THE SOFTWARE.
'=================================================================================

Option Explicit
'===============================================================
'              Public Windows Control Functions
'===============================================================
'Windows Sleep program sub
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'Windows Set Focus command
Public Declare Function PutFocus Lib "user32" Alias "SetFocus" (ByVal hwnd As Long) As Long
'Windows send message to interrupt communication inside windows, allowing modification of windows elements
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Integer, ByVal lParam As Long) As Long
'Top-most form windows function and constants
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, y, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Const HWND_TOPMOST = -1
Private Const HWND_NOTOPMOST = -2
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1
Private Const TOPMOST_FLAGS = SWP_NOMOVE Or SWP_NOSIZE

'======================================================
'            Inp/Out functions decleration
'=====================================================
Public Declare Function Inp Lib "inpout32.dll" _
Alias "Inp32" (ByVal PortAddress As Integer) As Integer
Public Declare Sub Out Lib "inpout32.dll" _
Alias "Out32" (ByVal PortAddress As Integer, ByVal Value As Integer)

'===============================================================
'                           Variables
'===============================================================
'Application Information Variables
Public strSaveFile As String                'Path to desktop
Public strDebugSaveData As String           'Debug Save File
Public Application As New application_data  'App Data Class

'Game Data Variables
Public race As Boolean                      'Is race happenning? Bool
Public Highscore As New Highscore_Manager   'Highscore functions
Public Player1 As New Player_Data           'Player 1's Data class
Public Player2 As New Player_Data           'Player 2's Data class
Public totalLaps As Integer                 'Total Laps
Public timeArr(0 To 2, 0 To 1) As Integer   'Stopwatch array
Public timeLap As String                    'Stopwatch String Output
Public Settings As New Game_Settings        'Settings and options class
Public c As Integer                         'race start/Countdown Value
Public lights As Boolean                    'Flashing Track Lights

'Input / Output Command variables/constants
Public Const INPUT_ADDRESS = &H379          'Constant hex value for Input
Public Const OUTPUT_ADDRESS = &H378         'Constant hex value for Output
Public inVal As Long                        'Decimal In Value

'Input / Output value constants
Public Const PLAYER1_FAST = 0               'Decimal value for player 1 fast
Public Const PLAYER1_STOP = 0               'Decimal value for player 1 no drive
Public Const PLAYER2_FAST = 0               'Decimal value for player 2 fast
Public Const PLAYER2_STOP = 0               'Decimal value for player 2 no drive
Public Const TRACK_LIGHTS = 0               'Decimal value for track lights

'===============================================================
'        Function: MakeTopMost()
'           Input: hwnd - any form's frm.hwnd value (the form to control)
'          Return: None
'     Description: Forces window to top level
'===============================================================

Public Sub MakeTopMost(hwnd As Long)
    SetWindowPos hwnd, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS
End Sub

'===============================================================
'        Function: SetupApplication()
'           Input: None
'          Return: None
'     Description: Load all data to variables
'===============================================================
Public Sub SetupApplication()
    strSaveFile = GetPath(CSIDL_DESKTOP) & "\Debug" & Format(Now, "dd-mm-yy") & ".rsetg"
    Application.loadVariables
    Settings.getSettings
    printf "Ready SET Go! Racing Debugger!", False
    printf "(c) Copyright Ethan Elliott 2014", False
    printf "", False
    trackLights True
End Sub

'===============================================================
'        Function: CloseApp()
'           Input: None
'          Return: None
'     Description: Confirm application close and end all
'                  running games.
'===============================================================
Public Sub CloseApp()
    Dim intFrmExit As Integer                'Exit Application response
    intFrmExit = MsgBox("Are you sure you want to exit?", vbYesNo + vbQuestion, "Exit")
    If intFrmExit = vbYes Then
        Settings.saveSettings
        stopRace
        printf "Application Closed"
        Open strSaveFile For Output As #1
        Print #1, strDebugSaveData
        Close #1
        Sleep 1000
        End
    End If
    printf "Application Close Cancelled"
End Sub

'===============================================================
'        Function: loadRace()
'           Input: p1Name = Player1's Name (String)
'                  p2Name = Player2's Name (String)
'                  laps = Number of laps (Long)
'          Return: None
'     Description: Set all entered data about the race into
'                  variables.
'===============================================================
Public Sub loadRace(ByVal p1Name As String, ByVal p2Name As String, ByVal laps As Long)
    Dim iNetData As String
    resetRace
    totalLaps = laps
    Player1.Reset laps
    Player2.Reset laps
    Player1.Name = p1Name
    Player2.Name = p2Name
    Player1Drive (0)
    Player2Drive (0)
    iNetData = frmMain.iNet.OpenURL(("http://www.rsetg.ethanelliott.ca?code=" & Settings.mobileCode & "&p1n=" & Player1.Name & "&p2n=" & Player2.Name))
    printf "Race Loaded with " & laps & " laps"
End Sub

'===============================================================
'        Function: startRace()
'           Input: None
'          Return: None
'     Description: Begin the countdown
'===============================================================
Public Sub startRace()
    c = 1
    With frmMain
        .lblPlayer1Name.Caption = Player1.Name
        .lblPlayer2Name.Caption = Player2.Name
        .tmrCountDown.enabled = True
        .wmpBackground.URL = ""
        .wmpCountdownBeep.URL = Application.beeps
    End With
End Sub

'===============================================================
'        Function: startRaceGo()
'           Input: None
'          Return: None
'     Description: set race to true, and begin all race-based
'                  activities.
'===============================================================
Public Sub startRaceGo()
    race = True
    Player1.race = True
    Player2.race = True
    With frmMain
        .wmpBackground.Settings.setMode "loop", True
        .wmpBackground.URL = Settings.backMusicFile
        .lblPlayer1Name.Caption = Player1.Name
        .lblPlayer2Name.Caption = Player2.Name
        .tmrStopwatch.enabled = True
        '.imgStopRace.Visible = True
        .SelectBox.SetFocus
        .cmdStartRace.enabled = False
        .cmbLapNum.enabled = False
        .txtPlayer1.enabled = False
        .txtPlayer2.enabled = False
        .tmrPlayer1.enabled = Player1.race
        .tmrPlayer2.enabled = Player2.race
    End With
    printf "Race started!"
End Sub

'===============================================================
'        Function: stopRace()
'           Input: None
'          Return: None
'     Description: stops the race, and removes control of the
'                  vehicles.
'===============================================================
Public Sub stopRace()
    race = False
    Player1.race = False
    Player2.race = False
    Player1Drive (0)
    Player2Drive (0)
    With frmMain
        .tmrStopwatch.enabled = False
        .SelectBox.SetFocus
        .imgStopRace.Visible = False
        .cmdStartRace.enabled = True
        .cmbLapNum.enabled = True
        .txtPlayer1.enabled = True
        .txtPlayer2.enabled = True
        .tmrPlayer1.enabled = Player1.race
        .tmrPlayer2.enabled = Player2.race
    End With
printf "Race Stopped"
End Sub

'===============================================================
'        Function: resetRace()
'           Input: None
'          Return: None
'     Description: Reset all variables associated with the race,
'                  and reset display labels, in preperation for
'                  next race.
'===============================================================
Public Sub resetRace()
    Dim a As Integer
    Dim b As Integer
    c = 0
    race = False
    Player1.Reset 0
    Player2.Reset 0
    Player1Drive (0)
    Player2Drive (0)
    With frmMain
        .tmrStopwatch.enabled = False
        .SelectBox.SetFocus
        .cmdStartRace.enabled = True
        .cmbLapNum.enabled = True
        .txtPlayer1.enabled = True
        .txtPlayer2.enabled = True
        .tmrPlayer1.enabled = Player1.race
        .tmrPlayer2.enabled = Player2.race
        For a = 0 To 2 Step 1
            For b = 0 To 1 Step 1
                timeArr(a, b) = 0
            Next
        Next
    End With
printf "Race Reset"
End Sub

'===============================================================
'        Function: printf()
'           Input: output = String to print (String)
'                  time = T/F Display time on line (Boolean) *OPTIONAL
'          Return: None
'     Description: print a line of text to the debug window.
'===============================================================
Public Sub printf(ByVal outPut As String, Optional time As Boolean = True)
    With frmDebug
        If time Then
            .txtDebugOut.Text = .txtDebugOut.Text & vbNewLine & Now & " - " & outPut
        Else
            .txtDebugOut.Text = .txtDebugOut.Text & vbNewLine & outPut
        End If
        strDebugSaveData = .txtDebugOut.Text
    End With
End Sub

'===============================================================
'        Function: Player1Drive()
'           Input: Speed = Desired speed of the car (Integer)
'          Return: None
'     Description: Change / Modify output and display to
'                  match desired car speed.
'===============================================================
Public Sub Player1Drive(ByVal Speed As Integer)
Player1.Speed = Speed
If Speed = 0 Then
    frmMain.imgPlayer1Speed(2).Visible = True
    frmMain.imgPlayer1Speed(1).Visible = False
    frmMain.imgPlayer1Speed(0).Visible = False
ElseIf Speed = 1 Then
    frmMain.imgPlayer1Speed(2).Visible = False
    frmMain.imgPlayer1Speed(1).Visible = False
    frmMain.imgPlayer1Speed(0).Visible = True
End If
End Sub

'===============================================================
'        Function: Player2Drive()
'           Input: Speed = Desired speed of the car (Integer)
'          Return: None
'     Description: Change / Modify output and display to
'                  match desired car speed.
'===============================================================
Public Sub Player2Drive(ByVal Speed As Integer)
Player2.Speed = Speed
If Speed = 0 Then
    frmMain.imgPlayer2Speed(2).Visible = True
    frmMain.imgPlayer2Speed(1).Visible = False
    frmMain.imgPlayer2Speed(0).Visible = False
ElseIf Speed = 1 Then
    frmMain.imgPlayer2Speed(2).Visible = False
    frmMain.imgPlayer2Speed(1).Visible = False
    frmMain.imgPlayer2Speed(0).Visible = True
End If
End Sub

'===============================================================
'        Function: SETOut()
'           Input: None
'          Return: None
'     Description: Adds the currently active outputs and
'                  returns them
'===============================================================
Public Function SETOut() As Integer
    Dim outValue As Integer     'Create function return value
    outValue = 0                'Make sure the value is always 0
    'Player1 Speed Output
    If Player1.Speed = 0 Then
        outValue = outValue + PLAYER1_STOP
    ElseIf Player1.Speed = 2 Then
        outValue = outValue + PLAYER1_FAST
    End If
    
    'Player2 Speed Output
    If Player2.Speed = 0 Then
        outValue = outValue + PLAYER2_STOP
    ElseIf Player2.Speed = 2 Then
        outValue = outValue + PLAYER2_FAST
    End If
    If lights Then
        outValue = outValue + TRACK_LIGHTS
    End If
    SETOut = outValue   'Return the value
End Function

'===============================================================
'        Function: timeToLng()
'           Input: "time" string including numbers and ":"
'          Return: time as long without ":"
'     Description: Converts a timer string to a long number
'                  for math operands
'===============================================================
Public Function timeToLng(ByVal time As String) As Long
    Dim Data() As String
    Dim min As String
    Dim sec As String
    Dim mil As String
    Dim reVal As Long
    Data() = Split(time, ":")
    min = Data(0)
    sec = Data(1)
    mil = Data(2)
    reVal = CLng(min & sec & mil)
    timeToLng = reVal
End Function

'===============================================================
'        Function: lngToTime()
'           Input: lng long value representing time
'          Return: time as string including ":"
'     Description: converts a long time value to a string
'                  with ":"
'===============================================================
Public Function lngToTime(ByVal lng As Long) As String
    Dim Data As String
    Dim min As String
    Dim sec As String
    Dim mil As String
    Dim reVal As String
    Data = Format(lng, "000000")
    mil = Right(Data, 2)
    sec = Mid(Data, 3, 2)
    min = Left(Data, 2)
    reVal = min & ":" & sec & ":" & mil
    lngToTime = reVal
End Function
'End

Public Sub trackLights(ByVal enabled As Boolean, Optional Speed As Long = 1000)
    If enabled Then
        With frmMain.tmrFlashingLights
            .enabled = True
            .Interval = Speed
        End With
        lights = True
    Else
        With frmMain.tmrFlashingLights
            .enabled = False
            .Interval = Speed
        End With
        lights = False
    End If
End Sub
