VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "About"
   ClientHeight    =   5400
   ClientLeft      =   2340
   ClientTop       =   1935
   ClientWidth     =   6615
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Century Gothic"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3727.176
   ScaleMode       =   0  'User
   ScaleWidth      =   6211.827
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEULA 
      Caption         =   "Liscence Agreement"
      Height          =   398
      Left            =   120
      TabIndex        =   5
      Top             =   4920
      Width           =   6375
   End
   Begin VB.Label lblDescription 
      BackStyle       =   0  'Transparent
      Caption         =   "Brief Description:"
      Height          =   1935
      Left            =   240
      TabIndex        =   4
      Top             =   3120
      Width           =   5415
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblDevelopers 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Developers:"
      Height          =   330
      Left            =   2040
      TabIndex        =   3
      Top             =   2280
      Width           =   1635
   End
   Begin VB.Label lblRelease 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Release: "
      Height          =   330
      Left            =   2040
      TabIndex        =   2
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Version: "
      Height          =   330
      Left            =   2040
      TabIndex        =   1
      Top             =   840
      Width           =   1155
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Ready S.E.T Go Racing!"
      BeginProperty Font 
         Name            =   "Century Gothic"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   6585
   End
   Begin VB.Image Image1 
      Height          =   1815
      Left            =   120
      Picture         =   "frmAbout.frx":0442
      Stretch         =   -1  'True
      Top             =   840
      Width           =   1815
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'EULA Button Click
Private Sub cmdEULA_Click()
    frmEULA.Visible = True
End Sub

'Form Load
Private Sub Form_Load()
    MakeTopMost Me.hwnd
    lblVersion = "Version: " & Application.Version
    lblRelease = "Release: " & Application.Release
    lblDevelopers = "Developers: " & Application.Developers
    lblDescription = Application.Description
    printf "About Form Loaded"
End Sub
