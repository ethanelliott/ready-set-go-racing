# Ready S.E.T. Go Racing Simulator! #

This is the official repo for Ready S.E.T. Go Racing Simulator.

### About Ready S.E.T. Go Racing Simulator ###

* Project Name: *Ready S.E.T. Go Racing Simulator*
* Version: *0.1.6*

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact